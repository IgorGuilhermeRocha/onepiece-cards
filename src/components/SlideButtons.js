import "../styles/buttons.css";
import { useNavigate } from 'react-router-dom';


function SlideButtons(props) {
    const navigate = useNavigate();

    return (
        <div onClick={() => { navigate("/" + IncrementId(props.value, props.id, props.maxValue)) }} className="slide-buttons" >
            <img src={props.image} alt="" />
        </div>
    );
}

function IncrementId(value, id, maxValue) {
    let newId = parseInt(id);

    if (value === "0") {
        if (newId > 0) {
            newId--;
        } else {
            newId = maxValue - 1;
        }
    } else if (value === "1" && newId < maxValue - 1) {
        newId++;
    } else {
        newId = 0;
    }
    return (newId);
}

export default SlideButtons;
