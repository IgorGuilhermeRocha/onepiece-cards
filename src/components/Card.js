import "../styles/card.css";

function Card(props) {

    let classCard = chooseBackground(props.bounty);


    return (
        <div className={classCard}>
            <img src={props.image} alt="" />
            <div className="name-and-bounty">
                <p className="pirate-name">{props.name}</p>
                <p className="pirate-bounty">{props.bounty}</p>
            </div>
        </div>
    );
}

function chooseBackground(bounty) {
    let numberBounty = parseFloat(bounty.replaceAll(",", ""));

    if (numberBounty > 5380000) {
        return "card-template-2";
    }
    return "card-template"
}

export default Card;
