import Syre from "../assets/imgs/Syre.jpeg";
import Fael from "../assets/imgs/fael.jpg";
import Chicao from "../assets/imgs/chicao.jpg";
import Milhas from "../assets/imgs/milhas.jpg";
import Alimaos from "../assets/imgs/alimaos.png";
import Samuel from "../assets/imgs/samuel.jpg";
import Gorma from "../assets/imgs/Gorma.jpeg";

export const PirateList = [
    {
        image: Syre,
        name: "Syre Syre",
        bounty: "1,000,000",
    },
    {
        image: Fael,
        name: "Nariz de mastro",
        bounty: "5,380,000",
    },

    {
        image: Samuel,
        name: "Homem Arroto",
        bounty: "6,500,999",
    },

    {
        image: Chicao,
        name: "Chicão AK trovão",
        bounty: "4,320,182",
    },
    {
        image: Milhas,
        name: "Mil houses",
        bounty: "500,189",
    },
    {
        image: Alimaos,
        name: "God hands",
        bounty: "80,002",
    }, {
        image: Gorma,
        name: "Wife Hunter",
        bounty: "5,500,160",
    }
]

