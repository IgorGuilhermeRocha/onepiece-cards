import Card from "../components/Card.js";
import { useParams } from "react-router-dom";
import { PirateList } from "../helpers/PirateList.js";
import SlideButtons from "../components/SlideButtons.js";
import Esquerda from "../assets/imgs/esquerda(1).png"
import Direita from "../assets/imgs/direita2(1).png"

function GetId() {
  let { id } = useParams();
  if (id === undefined) {
    id = 0;
  }
  return id;
}

function Index() {

  let id = GetId();

  const pirate = PirateList[id];

  return (
    <main className="main">
      <SlideButtons image={Esquerda} value="0" id={id} maxValue={PirateList.length} />
      <Card image={pirate.image} name={pirate.name} bounty={pirate.bounty} />
      <SlideButtons image={Direita} value="1" id={id} maxValue={PirateList.length} />
    </main>
  );
}

export default Index;
